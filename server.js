const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser');

const categories = require('./routes/api/categories')
const subcategories = require('./routes/api/subcategories')
const sheets = require('./routes/api/sheets')

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const db = require('./config/keys').mongoURI
mongoose
  .connect(db)
  .then(() => console.log('MongoDB Connected'))
  .catch((err => console.log(err)))

app.use('/api/categories', categories)
app.use('/api/subcategories', subcategories)
app.use('/api/sheets', sheets)

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

const port = process.env.PORT || 5000

app.listen(port, () => console.log(`Server running on port ${port}`))