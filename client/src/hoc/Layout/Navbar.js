import React from 'react';
import './Navbar.css';

const Navbar = () => {
  return <nav className="uk-navbar-container uk-light uk-margin" data-uk-navbar>
    <div className="uk-navbar-left">
      <ul className="uk-navbar-nav">
        <li><a href="#">Item</a></li>
        <li>
          <a href="#">Parent</a>
          <div className="uk-navbar-dropdown">
            <ul className="uk-nav uk-navbar-dropdown-nav">
              <li className="uk-active"><a href="#">Active</a></li>
              <li><a href="#">Item</a></li>
              <li><a href="#">Item</a></li>
            </ul>
          </div>
        </li>
        <li><a href="#">Item</a></li>
      </ul>
    </div>
    <div className="uk-navbar-right">
      <ul className="uk-navbar-nav">
        <li><a href="#">Item</a></li>
        <li>
          <a href="#">Parent</a>
          <div className="uk-navbar-dropdown">
            <ul className="uk-nav uk-navbar-dropdown-nav">
              <li className="uk-active"><a href="#">Active</a></li>
              <li><a href="#">Item</a></li>
              <li><a href="#">Item</a></li>
            </ul>
          </div>
        </li>
        <li><a href="#">Item</a></li>
      </ul>
    </div>
  </nav>
}

export default Navbar