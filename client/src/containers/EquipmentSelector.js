import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Input from '../components/Forms/Input';

import { ClothingHead } from '../assets/clothing/head';
import { ClothingTorso } from '../assets/clothing/torso';
import { ClothingLegs } from '../assets/clothing/legs';
import { ClothingFeet } from '../assets/clothing/feet';

import { ArmourTorso } from '../assets/armour/torso';

class EquipmentSelector extends Component {
  render() {
    return <div className="uk-card uk-card-default uk-card-body">
      <ul data-uk-accordion>
        <li className="uk-open">
          <a className="uk-accordion-title" href="#">Clothing</a>
          <div className="uk-accordion-content">
            <div data-uk-grid>
              <div className="uk-width-1-4">
                <ul className="uk-tab-left" data-uk-tab="connect: #clothing-tab; animation: uk-animation-fade">
                  <li><a href="#">Torso</a></li>
                  <li><a href="#">Legs</a></li>
                  <li><a href="#">Feet</a></li>
                </ul>
              </div>
              <div className="uk-width-3-4">
                <ul id="clothing-tab" className="uk-switcher">
                  <li>
                    <div className="uk-child-width-1-5 uk-flex-bottom uk-text-center" data-uk-grid>
                      {ClothingTorso.map((item, index) => <div key={index}>
                        <label>
                          {item.images && <div>
                            <img src={item.images.body} alt={item.name} />
                          </div>}
                          <div className="uk-text-small">
                            {item.name}
                          </div>
                          <Field component={Input} name={item.input_name} type="radio" value={item.id} />
                        </label>
                      </div>)}
                    </div>
                  </li>
                  <li>
                    <div className="uk-child-width-1-5 uk-flex-bottom uk-text-center" data-uk-grid>
                      {ClothingLegs.map((item, index) => <div key={index}>
                        <label>
                          {item.images && <div>
                            <img src={item.images.body} alt={item.name} />
                          </div>}
                          <div className="uk-text-small">
                            {item.name}
                          </div>
                          <Field component={Input} name={item.input_name} type="radio" value={item.id} />
                        </label>
                      </div>)}
                    </div>
                  </li>
                  <li>
                    <div className="uk-child-width-1-5 uk-flex-bottom uk-text-center" data-uk-grid>
                      {ClothingFeet.map((item, index) => <div key={index}>
                        <label>
                          {item.images && <div>
                            <img src={item.images.body} alt={item.name} />
                          </div>}
                          <div className="uk-text-small">
                            {item.name}
                          </div>
                          <Field component={Input} name={item.input_name} type="radio" value={item.id} />
                        </label>
                      </div>)}
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </li>
        <li>
          <a className="uk-accordion-title" href="#">Armour</a>
          <div className="uk-accordion-content">
            <div data-uk-grid>
              <div className="uk-width-1-4">
                <ul className="uk-tab-left" data-uk-tab="connect: #armour-tab; animation: uk-animation-fade">
                  <li><a href="#">Torso</a></li>
                </ul>
              </div>
              <div className="uk-width-3-4">
                <ul id="armour-tab" className="uk-switcher">
                  <li>
                    <div className="uk-child-width-1-5 uk-flex-bottom uk-text-center" data-uk-grid>
                      {ArmourTorso.map((item, index) => <div key={index}>
                        <label>
                          {item.images && <div>
                            <img src={item.images.body} alt={item.name} />
                          </div>}
                          <div className="uk-text-small">
                            {item.name}
                          </div>
                          <Field component={Input} name={item.input_name} type="radio" value={item.id} />
                        </label>
                      </div>)}
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  }
}

EquipmentSelector = reduxForm({
  form: 'equipmentForm',
  enableReinitialize: true
})(EquipmentSelector);

export default EquipmentSelector;