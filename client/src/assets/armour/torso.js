import Empty from '../../images/empty.png';

const inputName = 'armour_torso';

export const ArmourTorso = [{
  id: '0',
  images: {
    face: Empty,
    body: Empty
  },
  input_name: inputName,
  name: 'None'
}, {
  id: '1',
  images: {
    face: Empty,
    body: 'https://res.cloudinary.com/shogi/image/upload/q_auto:best/v1526574438/spritemake/Armor_-_Steel_Mail_-_Idle.png'
  },
  input_name: inputName,
  name: 'Steel Mail'
}, {
  id: '2',
  images: {
    face: Empty,
    body: 'https://res.cloudinary.com/shogi/image/upload/q_auto:best/v1526574438/spritemake/Armor_-_Gold_Mail_-_Idle.png'
  },
  input_name: inputName,
  name: 'Gold Mail'
}];