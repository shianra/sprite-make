import Empty from '../../images/empty.png';

const inputName = 'base_hair';

export const BaseHair = [{
  id: '0',
  images: {
    face: Empty,
    body: Empty
  },
  input_name: inputName,
  name: 'None'
}, {
  id: '1',
  images: {
    face: Empty,
    body: 'https://res.cloudinary.com/shogi/image/upload/q_auto:best/v1526574438/spritemake/Hair_-_Brown_Onion_-_Idle.png'
  },
  input_name: inputName,
  name: 'Brown Onion'
}, {
  id: '2',
  images: {
    face: Empty,
    body: 'https://res.cloudinary.com/shogi/image/upload/q_auto:best/v1526574438/spritemake/Hair_-_Purple_Onion_-_Idle.png'
  },
  input_name: inputName,
  name: 'Purple Onion'
}];