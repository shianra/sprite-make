import Empty from '../../images/empty.png';

const inputName = 'clothing_head';

export const ClothingHead = [{
  id: '0',
  images: {
    face: Empty,
    body: Empty
  },
  input_name: inputName,
  name: 'None'
}]