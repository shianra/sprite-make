import Empty from '../../images/empty.png';

const inputName = 'clothing_feet';

export const ClothingFeet = [{
  id: '0',
  images: {
    face: Empty,
    body: Empty
  },
  input_name: inputName,
  name: 'None'
}, {
  id: '1',
  images: {
    face: Empty,
    body: 'https://res.cloudinary.com/shogi/image/upload/q_auto:best/v1526574438/spritemake/Boots_-_Leather_-_Idle.png'
  },
  input_name: inputName,
  name: 'Leather Boots'
}];