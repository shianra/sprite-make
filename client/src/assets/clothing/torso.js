import Empty from '../../images/empty.png';

const inputName = 'clothing_torso';

export const ClothingTorso = [{
  id: '0',
  images: {
    face: Empty,
    body: Empty
  },
  input_name: inputName,
  name: 'None'
}, {
  id: '1',
  images: {
    face: Empty,
    body: 'https://res.cloudinary.com/shogi/image/upload/q_auto:best/v1526574439/spritemake/Shirt_-_Green_-_Idle.png'
  },
  input_name: inputName,
  name: 'Green Shirt'
}, {
  id: '2',
  images: {
    face: Empty,
    body: 'https://res.cloudinary.com/shogi/image/upload/q_auto:best/v1526574439/spritemake/Shirt_-_Purple_-_Idle.png'
  },
  input_name: inputName,
  name: 'Purple Shirt'
}];