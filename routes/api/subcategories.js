const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

const Subcategory = require('../../models/Subcategory')

// @route   GET api/categories
// @desc    Get subcategories
// @access  Public
router.get('/', (req, res) => {
  Subcategory.find()
    .then(subcategories => res.json(subcategories))
    .catch(err => res.status(404).json({ nosubcategoriesfound: 'No subcategories found' }))
})

module.exports = router