const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

const Category = require('../../models/Category')

// @route   GET api/categories
// @desc    Get categories
// @access  Public
router.get('/', (req, res) => {
  Category.find()
    .then(categories => res.json(categories))
    .catch(err => res.status(404).json({ nocategoriesfound: 'No categories found' }))
})

module.exports = router